const path = require('path');
const express = require('express');
const app = express();
const PORT = 9000;
const PATH_DIR = __dirname + '/public';

app.use(express.static('public'));

app.get('/', (req, res) => {
    res.send("Hello HTTP Server");
});

app.get('/home', (req, res) => {
    res.sendFile(path.join(PATH_DIR + 'chapter4.html'));
});

app.get('/cari-mobil', (req, res) => {

});

app.listen(PORT, () => console.log('Server running at localhost:${PORT}'));